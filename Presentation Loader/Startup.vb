﻿Imports Presentation_Loader.LoaderClass
Module Startup
    Public app As String = My.Application.Info.AssemblyName
    Public Log As New ErrorLogger

    Sub Main()
        ' Console App: Presentation Loader.exe
        ' Description: This application is ran on the HR Presentation TV computer via a Scheduled Task.  Using this console app it is possible to automate PowerPoint presentations to load into slideshow mode automaticall.
        ' Author: Joel Davis

        Console.Title = app

        Try
            For Each arg As String In My.Application.CommandLineArgs
                Select Case Trim(LCase(arg))
                    Case "/movefile"
                        NewSShow()

                    Case "/startslides"
                        StartSShow()

                    Case Else
                        End
                End Select
            Next
        Catch ex As Exception
            Try
                Log.Errors(app, True, ex.Message & " " & ex.StackTrace, )
            Catch ex2 As Exception
                Log.Errors(app, False, ex.Message & " " & ex.StackTrace, "DB Logging Issue: " & ex2.Message & " " & ex2.StackTrace)
            End Try
        End Try
    End Sub

End Module
