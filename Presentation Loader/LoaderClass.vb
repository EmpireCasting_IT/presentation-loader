﻿Imports System.IO
Imports System.Threading
Imports System.Configuration

Public Class LoaderClass
    Shared ReadOnly ppointPath As String = ConfigurationManager.AppSettings("PPointPath")
    Shared ReadOnly editQueuePath As String = ConfigurationManager.AppSettings("EditQueuePath")
    Shared ReadOnly newPPointPath As String = ConfigurationManager.AppSettings("NewPPointPath")
    Shared ReadOnly archiveDir As String = ConfigurationManager.AppSettings("ArchiveDir")
    Shared ReadOnly datetimeFormat As String = "(" & Now.ToString("MM-dd-yyyy HH.mm.ss") & ")"
    Shared startupFile As String = Nothing
    Shared ReadOnly fileDir As New IO.DirectoryInfo(ppointPath)
    Shared ReadOnly oldPPointArray As IO.FileInfo() = fileDir.GetFiles("*.pptx")
    Shared ReadOnly editDir As New IO.DirectoryInfo(editQueuePath)
    Shared ReadOnly editPPointArray As IO.FileInfo() = editDir.GetFiles("*.pptx")
    Shared ReadOnly newDir As New IO.DirectoryInfo(newPPointPath)
    Shared ReadOnly newPPointArray As IO.FileInfo() = newDir.GetFiles("*.pptx")
    Shared PPoint As IO.FileInfo = Nothing

    Shared Sub NewSShow()
        Try
            Dim ppProcesses() As Process = Process.GetProcessesByName("POWERPNT")
            Dim oldPPoint As FileInfo = Nothing

            ' Check if folders exist.  If not, create them.
            If Not My.Computer.FileSystem.DirectoryExists(editQueuePath) Then
                Directory.CreateDirectory(editQueuePath)
            End If
            If Not My.Computer.FileSystem.DirectoryExists(newPPointPath) Then
                Directory.CreateDirectory(newPPointPath)
            End If
            If Not My.Computer.FileSystem.DirectoryExists(archiveDir) Then
                Directory.CreateDirectory(archiveDir)
            End If

            ' Terminate current PowerPoint session.
            For Each prog As Process In ppProcesses
                prog.Kill()
            Next

            Thread.Sleep(5000)

            ' Check if new file exists in the Edit Queue.  If so, replace the current PowerPoint with the modified version.
            If editPPointArray.Length > 0 Then
                ' Display to user what we are doing.
                Console.WriteLine("Editing PowerPoint...")

                For i = 0 To editPPointArray.Length - 1 Step 1
                    If Not editPPointArray(i).Name.Contains("~$") Then
                        PPoint = editPPointArray(i)

                        My.Computer.FileSystem.MoveFile(PPoint.FullName, ppointPath & PPoint.Name, True)
                    End If
                Next
            End If

            ' Check if new file exists in the New Presentation Queue.  If so, replace the current PowerPoint with the new version.
            If newPPointArray.Length > 0 Then
                ' Display to user what we are doing.
                Console.WriteLine("Updating to new PowerPoint...")

                For i = 0 To newPPointArray.Length - 1 Step 1
                    If Not newPPointArray(i).Name.Contains("~$") Or Not oldPPointArray(i).Name.Contains("~$") Then
                        oldPPoint = oldPPointArray(i)
                        PPoint = newPPointArray(i)

                        ' Move and Rename the current PowerPoint to Archive directory.
                        My.Computer.FileSystem.MoveFile(oldPPoint.FullName, archiveDir & oldPPoint.Name.Substring(0, oldPPoint.Name.LastIndexOf(".")) & " " & datetimeFormat & ".pptx", True)

                        ' Move new PowerPoint to statupFile PowerPoint directory.
                        My.Computer.FileSystem.MoveFile(PPoint.FullName, ppointPath & PPoint.Name, True)
                    End If
                Next
            End If

            ' Run the new or old Slide Show.
            StartSShow()

        Catch ex As Exception
            ' Run the old Slide Show.
            StartSShow()

            Try
                Log.Errors(app, True, ex.Message & " " & ex.StackTrace, )
            Catch ex2 As Exception
                Log.Errors(app, False, ex.Message & " " & ex.StackTrace, "DB Logging Issue: " & ex2.Message & " " & ex2.StackTrace)
            End Try
        End Try
    End Sub

    Shared Sub StartSShow()
        Try
            Dim ppProcesses() As Process = Process.GetProcessesByName("POWERPNT")

            ' Display to user what we are doing.
            Console.WriteLine("Starting PowerPoint...")

            ' Check to see if current slide is running.  If so, then do nothing.
            If ppProcesses.Count = 0 Then

                ' Must reload array to account for moved files.
                Dim newfileDir As New IO.DirectoryInfo(ppointPath)
                Dim PPointArray As IO.FileInfo() = newfileDir.GetFiles("*.pptx")

                ' Find the last PowerPoint file and start the Slide Show.

                If startupFile = Nothing Or PPointArray.Length > 1 Then
                    For i = 0 To PPointArray.Length - 1 Step 1
                        If PPointArray(i).LastWriteTime > PPointArray(PPointArray.Length - 1).LastWriteTime And Not PPointArray(i).Name.Contains("~$") Then
                            startupFile = PPointArray(i).FullName
                        End If
                    Next

                    If startupFile = Nothing Then
                        startupFile = PPointArray(PPointArray.Length - 1).FullName
                    End If
                ElseIf startupFile = Nothing Then
                    startupFile = PPointArray(PPointArray.Length - 2).FullName
                End If

                Process.Start("POWERPNT", "/s """ & startupFile & """")

                ' For Debugging: Log what file POWERPNT opens.
                'Log.Errors(app, False, startupFile, )
            Else
                End
            End If

        Catch ex As Exception
            Try
                Log.Errors(app, True, ex.Message & " " & ex.StackTrace, )
            Catch ex2 As Exception
                Log.Errors(app, False, ex.Message & " " & ex.StackTrace, "DB Logging Issue: " & ex2.Message & " " & ex2.StackTrace)
            End Try
        End Try
    End Sub
End Class
