﻿Imports System.IO
Imports System.Configuration

Public Class ErrorLogger
    Dim errorDir As String = ConfigurationManager.AppSettings("ErrorDir")
    Dim datetimeFormat As String = "(" & Now.ToString("MM-dd-yyyy HH.mm.ss") & ")"

    ''' <summary>
    ''' Logger.Errors enables you to easily log errors via error log database table, error log file, or email.
    ''' </summary>
    ''' <param name="app">A defined public variable in the Startup Object that contains the name of the application via 'My.Application.Info.AssemblyName'.</param>
    ''' <param name="logType">Either True or False.  This will allow for file or database based error logging.</param>
    ''' <param name="exception">Usually the 'ex.Message' or 'ex.StakcTrace' recieved from a catch statement, but custom exceptions can also be passed.</param>
    ''' <param name="helpDescription">Helpful information to help resolve the issue either by the user or the I.T. Department.</param>
    Public Sub Errors(app As String, logType As String, exception As String, Optional helpDescription As String = Nothing)
        Select Case logType
            Case "DB"
                ' Log errors to Database.
                Try
                    '  ---- Replace when Database has been created. ----
                    FileLog(app, exception, helpDescription)

                Catch ex As Exception
                    FileLog(app, exception, helpDescription & vbNewLine & vbNewLine & ex.StackTrace)
                End Try
            Case "Email"
                EmailLog(app, exception, helpDescription)

            Case Else
                FileLog(app, exception, helpDescription)
        End Select
    End Sub

    Private Sub EmailLog(app As String, exception As String, Optional helpDescription As String = Nothing)
        If Not My.Computer.FileSystem.DirectoryExists(errorDir) Then
            Directory.CreateDirectory(errorDir)
        End If

        ' Log errors to file.
        File.WriteAllText(errorDir & "Errors " & datetimeFormat & ".txt", "Program: " & app & vbNewLine &
                                                                             "Date: " & Now & vbNewLine & vbNewLine &
                                                                             "Exception: " & exception & vbNewLine & vbNewLine &
                                                                             "Help Description: " & helpDescription)
    End Sub

    Private Sub FileLog(app As String, exception As String, Optional helpDescription As String = Nothing)
        If Not My.Computer.FileSystem.DirectoryExists(errorDir) Then
            Directory.CreateDirectory(errorDir)
        End If

        ' Log errors to file.
        File.WriteAllText(errorDir & "Errors " & datetimeFormat & ".txt", "Program: " & app & vbNewLine &
                                                                             "Date: " & Now & vbNewLine & vbNewLine &
                                                                             "Exception: " & exception & vbNewLine & vbNewLine &
                                                                             "Help Description: " & helpDescription)
    End Sub
End Class
